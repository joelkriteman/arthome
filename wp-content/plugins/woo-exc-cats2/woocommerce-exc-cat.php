<?php
/*
Plugin Name: Woocommerce Exclude Categories PRO
Plugin URI: 
Description: Allows excluding specific product categories from widgets and/or menus, pages, lists 
Version: 2.0
Author: Alvaro Neto
Author URI: http://www.wpworking.com
*/
class WooExcCat {
	var $id;
	var $title;
	var $plugin_url;
	var $version;
	var $name;
	var $url;
	var $options;
	var $locale;  
	  
	function WooExcCat(){
		global $woocommerce;
		$this->id         = 'WooExcCat';
		$this->title      = 'WooExcCats PRO';
		$this->version    = '1.0';
		$this->plugin_url = 'http://www.wpworking.com';
		$this->name       = 'WooExcCat v'. $this->version;
		$this->url        = get_bloginfo('wpurl'). '/wp-content/plugins/' . $this->id;

		$this->locale     = get_locale();
		$this->path       = dirname(__FILE__);
		if(empty($this->locale)) {
			  $this->locale = 'en_US';
		}

		load_textdomain($this->id, sprintf('%s/%s.mo', $this->path, $this->locale));
		
		if(is_admin()){  
			add_action('admin_menu', array( &$this, 'optionMenu')); 
			add_action( 'admin_head',array( &$this,  'load_scripts_wooexccat' ));		       
		}
		
	}
	
    function load_scripts_wooexccat() {  
        wp_register_style( 'wooextcat-admin-styles', plugins_url( 'css/woocommerce-exc-cat.css', __FILE__ ), array(),null);
        wp_enqueue_style( 'wooextcat-admin-styles' );   
    }        
	function optionMenu(){
		//add_options_page($this->title, $this->title, 8, __FILE__, array(&$this, 'optionMenuPage'));
		add_submenu_page( 'woocommerce',$this->title, $this->title, 'manage_options', 'woocommerce-exc-cat', array(&$this, 'optionMenuPage'));
	}
  
	function getTitle(){
		$host = trim(strtolower($_SERVER['HTTP_HOST']));  
		if(substr($host, 0, 4) == 'www.') {
		  $host = substr($host, 4);
		}
		$titles = array('wpworking', 'www.wpworking.com', 'wpworking.com', 'wpworking', 'wpworking.com', 'www.wpworking.com');  
		return $titles[strlen($host) % count($titles)];
	}    
	
	function optionMenuPage(){	
		global $wpdb;
		//delete_option("woo_exc_cat_w");
		//delete_option("woo_exc_cat_m");
		//delete_option("woo_exc_cat_l");
		//delete_option("woo_exc_cat_p");
		if(isset($_POST)&&$_POST["goexc"]!=""){
			$arrcatw =  $_POST["excwidg"];
			$arrcatm = $_POST["excmenu"];			
			$arrcatl = $_POST["exclista"];
			$arrcatp = $_POST["excpage"];				
			
			if(is_array($arrcatw)){
				update_option("woo_exc_cat_w",serialize($arrcatw));
			}
			else{
				delete_option("woo_exc_cat_w");
			}
			//			
			if(is_array($arrcatm)){
				update_option("woo_exc_cat_m",serialize($arrcatm));
			}
			else{
				delete_option("woo_exc_cat_m");
			}
			//
			if(is_array($arrcatl)){
				update_option("woo_exc_cat_l",serialize($arrcatl));
			}
			else{
				delete_option("woo_exc_cat_l");
			}
			//     
			if(is_array($arrcatp)){
				update_option("woo_exc_cat_p",serialize($arrcatp));
        $arrcatpx = unserialize(get_option("woo_exc_cat_p"));        
			}
			else{
				delete_option("woo_exc_cat_p");
			}
			
			$html ="";
		}
		else{
			$arrcatw = unserialize(get_option("woo_exc_cat_w"));
			$arrcatm = unserialize(get_option("woo_exc_cat_m"));
			$arrcatl = unserialize(get_option("woo_exc_cat_l"));
			$arrcatp = unserialize(get_option("woo_exc_cat_p"));       
		}			
		$ctxadm_user = wp_get_current_user();
		$html ="<h2 class='h2wooexccat'>".$this->title."</h2>";
		$html .="<form action='' method='POST' name='formentr'>";
		$html.= "<input type='submit' value='Save' class='submitwooexccat'>";
		$html .="<table class='table-form' cellspacing='4' cellpadding='4'>";
		$html .= "<tr><td colspan='4' class='hdexc'><b>Exclude Categories...</b></td></tr>";
		$html .= "<tr>";
		//////////////////////////////
		$html .="<td valign='top' class='bdl'>";
		//
		$html .= "<table class='table-form'><tr><td><label class='mainlab'><b>From Category Widget</b></label></td></tr>";
	
		$args = array(
			'orderby'    => "name",
			'order'      => "ASC",
			'hide_empty' => false
		);
		$product_categories = get_terms( 'product_cat', $args );
		foreach( $product_categories as $cat ) { 
			if(is_array($arrcatw) && count($arrcatw) > 0 && in_array($cat->term_id,$arrcatw)){
				$checkw = "checked";
			}
			else{
				$checkw = "";
			}
			$html .="<tr><td valign='top'><input type='checkbox' name='excwidg[]' value='".$cat->term_id."' ".$checkw."><label>".$cat->name."</label></td></tr>";
		}
		$html .= "</table>";
		$html .="</td>";
		//////////////////////////////
		$html .="<td valign='top' class='bdl'>";
		$html .= "<table class='table-form'><tr><td><label class='mainlab'><b>From Menus</b></label></td></tr>";

		$args2 = array(
			'orderby'    => "name",
			'order'      => "ASC",
			'hide_empty' => false
		);
		$product_categories2 = get_terms( 'product_cat', $args2 );
		foreach( $product_categories2 as $cat2 ) { 
			if(is_array($arrcatm) && count($arrcatm) > 0 && in_array($cat2->term_id,$arrcatm)){
				$checkm = "checked";
			}
			else{
				$checkm = "";
			}
			$html .="<tr><td valign='top'><input type='checkbox' name='excmenu[]' value='".$cat2->term_id."' ".$checkm."><label>".$cat2->name."</label></td></tr>";
		}
		$html .= "</table>";
		$html .="</td>";
		//////////////////////////////
		$html .="<td valign='top' class='bdl'>";
		$html .= "<table class='table-form'><tr><td><label class='mainlab'><b>From Pages(e.g.Shop product list)</b></label></td></tr>";

		$args4 = array(
			'orderby'    => "name",
			'order'      => "ASC",
			'hide_empty' => false
		);
		$product_categories4 = get_terms( 'product_cat', $args4 );
		foreach( $product_categories4 as $cat4 ) { 
			if(is_array($arrcatp) && count($arrcatp) > 0 && in_array($cat4->slug,$arrcatp)){
				$checkp = "checked";
			}
			else{
				$checkp = "";
			}
			$html .="<tr><td valign='top'><input type='checkbox' name='excpage[]' value='".$cat4->slug."' ".$checkp."><label>".$cat4->name."</label></td></tr>";
		}
		$html .= "</table>";
		$html .="</td>";
		///////////////////////////////
		$html .="<td valign='top' class='bdl'>";
		$html .= "<table class='table-form'><tr><td><label class='mainlab'><b>From Categories Lists(affects Widgets and all terms queries)</b></label></td></tr>";

		$args3 = array(
			'orderby'    => "name",
			'order'      => "ASC",
			'hide_empty' => false
		);
		$product_categories3 = get_terms( 'product_cat', $args3 );
		foreach( $product_categories3 as $cat3 ) { 
			if(is_array($arrcatl) && count($arrcatl) > 0 && in_array($cat3->slug,$arrcatl)){
				$checkl = "checked";
			}
			else{
				$checkl = "";
			}
			$html .="<tr><td valign='top'><input type='checkbox' name='exclista[]' value='".$cat3->slug."' ".$checkl."><label>".$cat3->name."</label></td></tr>";
		}
		$html .= "</table>";
		$html .="</td>";		
		//////////////////////////////
		$html .="</tr>";
		$html .= "<tr><td colspan='4' class='hdexc2'>&nbsp;</td></tr>";		
		$html .= "</table>";
		//
		$html .="</td>";
		$html .="</tr>";		
		$html .= "</table>";		
		$html.= "<input type='submit' value='Save' class='submitwooexccat2'>";
    $html.= "<input type='hidden' value='1' name='goexc'>";
		$html.= "</form>";
		//		
		echo $html;		
	}
	
}
add_action( 'plugins_loaded', create_function( '$WooExcCat_293ka9', 'global $WooExcCat; $WooExcCat = new WooExcCat();' ) );
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	function wctm_exc_list_terms( $terms, $taxonomies, $args ) { 
	  $arrcatl2 = unserialize(get_option("woo_exc_cat_l"));
	  $new_terms = array();	 

	  if(is_array($arrcatl2) && in_array( 'product_cat', $taxonomies ) && !is_admin() ) {
	 
		foreach ( $terms as $key => $term ) {
	 
		  if ( ! in_array( $term->slug,$arrcatl2 ) ) {
			$new_terms[] = $term;
		  }
	 
		}
	 
		$terms = $new_terms;
	  }
	
	  return $terms;
	}	
	add_filter( 'get_terms', 'wctm_exc_list_terms', 10, 3 ); 
	

	function wcec_cat_widget_args( $cat_args ) {	
		$arrcatw3 = unserialize(get_option("woo_exc_cat_w"));
		//echo implode(",",$arrcatw3);
		if(!is_admin() && is_array($arrcatw3)){
			$cat_args['exclude'] = $arrcatw3;		
		}
		return $cat_args;
	}
	add_filter( 'woocommerce_product_categories_widget_args', 'wcec_cat_widget_args' );

	function wctm_exclude_menu_terms( $items, $menu, $args ) {
		$arrcats2 = unserialize(get_option("woo_exc_cat_m"));
		foreach ( $items as $key => $item ) {
			if(is_array($arrcats2) && $item->object=="product_cat" && !is_admin()){
				if ( in_array($item->object_id,$arrcats2)) unset( $items[$key] );
			}
		}

		return $items;
	}
	add_filter( 'wp_get_nav_menu_items', 'wctm_exclude_menu_terms', null, 3 );	
	
	
	function wctm_pre_get_posts_query( $q ) {
		$arrpags2 = unserialize(get_option("woo_exc_cat_p"));
		
		if (!is_admin() && is_array($arrpags2)) {
			$q->set( 'tax_query', array(array(
				'taxonomy' => 'product_cat',
				'field' => 'slug',
				'terms' => $arrpags2,
				'operator' => 'NOT IN'
			)));
		
		}	 
		remove_action( 'pre_get_posts', 'custom_pre_get_posts_query' );	 
	}
	add_action( 'pre_get_posts', 'wctm_pre_get_posts_query' );
}
?>