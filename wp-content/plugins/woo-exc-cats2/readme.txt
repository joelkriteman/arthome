=== Woocommerce Exclude Categories PRO 
===Contributors: alvaronDonate link: http://www.wpworking.com/
Tags: woocommerce, categories, exclude, products, widgets, menus
Requires at least: 3.6.1
Tested up to: 4.1
Stable tag: 2.0
License URI: http://www.gnu.org/licenses/gpl-2.0.html
== Description 
==Allows excluding specific product categories from widgets and/or menus, pages, lists
== Installation 
==1. Upload `woocommerce-exc-cat` folder to the `/wp-content/plugins/` directory2. 
Activate the plugin through the 'Plugins' menu in WordPress
== Frequently Asked Questions 
==If you have any questions, please let me know  wpworking@wpworking.com
== Screenshots 
==== Changelog 
==Versioning
1.1 fixed warnings on lines 129, 150, 171 e 192
2.0 improved post process
== Upgrade Notice 
==First Version
== Arbitrary section 
==More info about the plugin: http://www.wpworking.com/Ask for support on wpworking@wpworking.com