<?php

			$order = new WC_Order( $order_id );
			
			if ( class_exists( 'WC_Subscriptions' ) && WC_Subscriptions_Order::order_contains_subscription( $order_id ) ) {
				
				switch ( strtolower(WC_Subscriptions_Order::get_subscription_period( $order )) ) {
				
					case 'day' :
						$subscription_period = '1';
						break;
					
					case 'week' :
						$subscription_period = '2';
						break;
					
					case 'month' :
						$subscription_period = '3';
						break;
				
					case 'year' :
						$subscription_period = '4';
						break;
				
				}
				
				switch ( strtolower(WC_Subscriptions_Order::get_subscription_trial_period( $order )) ) {
				
					case 'day' :
						$trial_period = '1';
						break;
					
					case 'week' :
						$trial_period = '2';
						break;
					
					case 'month' :
						$trial_period = '3';
						break;
				
					case 'year' :
						$trial_period = '4';
						break;
				
				}
				
			}

			if ( $this->status == 'testing' ) {
				$worldpayform_adr 			= $this->testurl;
				$worldpay_args['testMode'] 	= '100';
			} else {
				$worldpayform_adr = $this->liveurl;
				$testMode		  = '';
			}
			if( $this->dynamiccallback ) {
				$callbackurl   = site_url( 'wp-content/plugins/woocommerce-gateway-worldpay/wpcallback.php' );
				$successurl    = site_url( 'wp-content/plugins/woocommerce-gateway-worldpay/wpcallback.php' );
			} else {
				$callbackurl   = str_replace( 'https:', 'http:', add_query_arg( 'wc-api', 'WC_Gateway_Worldpay_Form', home_url( '/' ) ) );
				$successurl    = str_replace( 'https:', 'http:', add_query_arg( 'wc-api', 'WC_Gateway_Worldpay_Form', home_url( '/' ) ) );
			}

			$failureurl    = str_replace( '&amp;','&',$order->get_cancel_order_url() );

			/**
			 * Sequential Order Numbers support
			 */ 
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			if ( get_post_meta( $order->id,'_order_number',TRUE ) && is_plugin_active( 'woocommerce-sequential-order-numbers/woocommerce-sequential-order-numbers.php' ) ) {
				$output_order_num = get_post_meta( $order->id,'_order_number',TRUE );
			} elseif ( get_post_meta( $order->id,'_order_number_formatted',TRUE ) && is_plugin_active( 'woocommerce-sequential-order-numbers-pro/woocommerce-sequential-order-numbers.php' ) ) {
				$output_order_num = get_post_meta( $order->id,'_order_number_formatted',TRUE );
			} else {
				$output_order_num = $order->id;
			}

			$worldpay_args['instId'] = $this->instId;
			$worldpay_args['cartId'] = str_replace( $this->clean_array, '',  $order->order_key . '-' . $output_order_num . '-' . time() );
			/**
			 * Modify the order amount for subscriptions
			 *
			 * If there is a subscription we get the amount from WC_Subscriptions_Order::get_total_initial_payment( $order )
			 * otherwise it's just the order total
			 *
			 * WC_Subscriptions_Order::get_total_initial_payment( $order ) works out if there is a payment due today, 
			 * if not this value will be 0 and no money will be taken today
			 */
			if( class_exists( 'WC_Subscriptions' ) && WC_Subscriptions_Order::order_contains_subscription( $order_id ) ) {
				$worldpay_args['amount'] = WC_Subscriptions_Order::get_total_initial_payment( $order );
			} else {
				$worldpay_args['amount'] = $order->order_total;
			}
			$worldpay_args['currency'] 	= get_woocommerce_currency();
			$worldpay_args['desc'] 		= str_replace( '{ordernum}', $output_order_num, str_replace( $this->clean_array, '',  $this->orderDesc ) );
			$worldpay_args['name'] 		= $order->billing_first_name. ' ' .$order->billing_last_name;
			$worldpay_args['address1'] 	= $order->billing_address_1;
			$worldpay_args['address2'] 	= $order->billing_address_2;
			$worldpay_args['address3'] 	= '';
			$worldpay_args['town'] 		= $order->billing_city;
			$worldpay_args['region'] 	= $order->billing_state;
			$worldpay_args['postcode'] 	= $order->billing_postcode;
			$worldpay_args['country'] 	= $order->billing_country;
			$worldpay_args['tel'] 		= $order->billing_phone;
			$worldpay_args['email'] 	= $order->billing_email;

			if ( $this->fixContact == 'yes' ) {
				$worldpay_args['fixContact'] = '';
			}

			if ( $this->hideContact == 'yes' ) {
				$worldpay_args['hideContact'] = '';
			}

			if ( $this->accid != '' || isset($this->accid) ) {
				$worldpay_args['accId1'] = $this->accid;
			}

			if ( $this->authMode == 'A' || $this->authMode == 'E' ) {
				$worldpay_args['authMode'] = $this->authMode;
			}

			if ( $this->hideCurrency == 'yes' ) {
				$worldpay_args['hideCurrency'] = '';
			}

			if ( $this->lang != '' || isset($this->lang) ) {
				$worldpay_args['lang'] = $this->lang;
			}

			if ( $this->noLanguageMenu == 'yes' ) {
				$worldpay_args['noLanguageMenu'] = '';
			}
			/**
			 * Adding a lot more form values for Subscriptions
			 */
			if ( class_exists( 'WC_Subscriptions' ) && WC_Subscriptions_Order::order_contains_subscription( $order_id ) ) {

				/**
				 * If subscription is for one period (1 month, 1 day, 1 year etc) and there is no trial period then we don't need to set up Future Pay
				 */
				if( WC_Subscriptions_Order::get_subscription_trial_length( $order ) < '1' && WC_Subscriptions_Order::get_subscription_length( $order ) == '1' ) {

				} else {

					$worldpay_args['futurePayType'] = 'regular';
					/**
					 * If the subscription period is less than 2 weeks the option must be 0 which means no modifications
					 */
					if ( $subscription_period == '1' || ( $subscription_period == '2' && WC_Subscriptions_Order::get_subscription_interval( $order ) <= '2' ) ) {
						$worldpay_args['option'] = 0;
					} else {
						$worldpay_args['option'] = 1;
					}
				
					/**
					 * Set start date if there is a trial period or use subscription period settings
					 * 
					 * Use strtotime because subscriptions passes an INT for the length and a word for period
					 * doing it any other way means a messy calculation
					 */
					if ( WC_Subscriptions_Order::get_subscription_trial_length( $order ) >= 1 || ( class_exists( 'WC_Subscriptions_Synchroniser' ) && WC_Subscriptions_Synchroniser::order_contains_synced_subscription( $order_id ) ) ) {

						if ( class_exists( 'WC_Subscriptions_Synchroniser' ) && WC_Subscriptions_Synchroniser::order_contains_synced_subscription( $order_id ) ) {
							// Get product
							$subscriptions_in_order      = WC_Subscriptions_Order::get_recurring_items( $order );
							$subscription_item           = array_pop( $subscriptions_in_order );
							$product_id                  = WC_Subscriptions_Order::get_items_product_id( $subscription_item );
							// Get first payment date
							$start_date = date( "Y-m-d", WC_Subscriptions_Synchroniser::get_first_payment_date( '', $order, $product_id, 'timestamp' ) );
						} else {
							$start_date = date("Y-m-d",strtotime("+" . WC_Subscriptions_Order::get_subscription_trial_length( $order ) . " " . WC_Subscriptions_Order::get_subscription_trial_period( $order )));
						}

						$worldpay_args['startDate'] = $start_date;

					} else {

						$worldpay_args['startDelayMult'] = WC_Subscriptions_Order::get_subscription_interval( $order );
						$worldpay_args['startDelayUnit'] = $subscription_period;							

					}
				
					/**
					 * Set subscription length
					 *
					 * WorldPay does not count the intial payment in the noOfPayments setting
					 *
					 * Includes work around for 2 payment subscriptions with no free trial.
					 */
					if( (WC_Subscriptions_Order::get_subscription_trial_length( $order ) == '0' || WC_Subscriptions_Order::get_subscription_trial_length( $order ) == '') && WC_Subscriptions_Order::get_subscription_length( $order ) == 2 ) {

						/**
						 * Two payment subscriptions with no free trial
						 * 
						 * Set the start date to be tomorrow, no payment will be taken initially
						 *
						 * Why do it this way?
						 * WorldPay takes 1 payment now so the number of payments in a subscription needs to be reduced by 1 BUT, 
						 * for a 2 payment subscription that means 1 payment now and 1 payment in the future - WorldPay does not allow only 1 payment in the future, the minimum is 2
						 * This work around means that the initial payment is take tomorrow at 3:00 AM, essentially forcing a free trial of 1 day.
						 *
						 * Further problems arise if the initial payment is not the same as the recurring payments.
						 */
						
						if ( WC_Subscriptions_Order::get_recurring_total( $order ) == WC_Subscriptions_Order::get_total_initial_payment( $order ) ) {
							$worldpay_args['amount'] = '0';
							unset( $worldpay_args['startDelayMult'] );
							unset( $worldpay_args['startDelayUnit'] );
							$worldpay_args['startDate'] = date( "Y-m-d", strtotime(date( "Y-m-d" ) . ' + 1 day') );
						} else {
							$worldpay_args['amount'] = WC_Subscriptions_Order::get_total_initial_payment( $order ) - WC_Subscriptions_Order::get_recurring_total( $order );
						}

						$worldpay_args['noOfPayments'] = WC_Subscriptions_Order::get_subscription_length( $order );

					} elseif( (WC_Subscriptions_Order::get_subscription_trial_length( $order ) == '0' || WC_Subscriptions_Order::get_subscription_trial_length( $order ) == '') && WC_Subscriptions_Order::get_subscription_length( $order ) > 2 ) {
						// More that two payments, no free trial
						$subs_length = WC_Subscriptions_Order::get_subscription_length( $order ) - 1;
						$worldpay_args['noOfPayments'] = $subs_length;

					} else {
						$worldpay_args['noOfPayments'] = WC_Subscriptions_Order::get_subscription_length( $order );
					}

					if ( WC_Subscriptions_Order::get_subscription_length( $order ) == '1') {

					} else {
						$worldpay_args['intervalMult'] = WC_Subscriptions_Order::get_subscription_interval( $order );
						$worldpay_args['intervalUnit'] = $subscription_period;	
					}

					$worldpay_args['normalAmount'] = WC_Subscriptions_Order::get_recurring_total( $order );

				} // if( WC_Subscriptions_Order::get_subscription_trial_length( $order ) == '0' && WC_Subscriptions_Order::get_subscription_length( $order ) == '1' )
			
			} // if ( class_exists( 'WC_Subscriptions' ) && WC_Subscriptions_Order::order_contains_subscription( $order_id ) )

			$worldpay_args['MC_callback'] 			= $callbackurl;
			$worldpay_args['MC_callback-ppe'] 		= $callbackurl;
			$worldpay_args['MC_SuccessURL'] 		= $successurl;
			$worldpay_args['MC_FailureURL'] 		= $failureurl;
			$worldpay_args['MC_order'] 				= $order->id;
			$worldpay_args['MC_transactionNumber'] 	= '1';

			/**
			 * Add MD5 args
			 *
			 * instId:amount:currency:cartId
			 */
			if ( $this->worldpaymd5 != '' ) {

				$worldpay_args['signatureFields'] = 'instId:amount:currency:cartId';

				$build_signature = $this->worldpaymd5.':'.$worldpay_args['instId'].':'.$worldpay_args['amount'].':'.$worldpay_args['currency'].':'.$worldpay_args['cartId'];

				$worldpay_args['signature'] = md5( $build_signature );

			}

			if ( function_exists( 'wc_enqueue_js' ) ) {
				wc_enqueue_js('
					jQuery(function(){
						jQuery("body").block({

							message: "<img src=\"'.$woocommerce->plugin_url().'/assets/images/ajax-loader.gif\" alt=\"Redirecting...\" />'.__('Thank you for your order. We are now redirecting you to WorldPay to make payment.', 'woocommerce_worlday').'",
							overlayCSS:
							{
								background: "#fff",
								opacity: 0.6
							},
							css:
							{
					        	padding:        20,
						        textAlign:      "center",
						        color:          "#555",
						        border:         "3px solid #aaa",
						        backgroundColor:"#fff",
						        cursor:         "wait",
						        lineHeight:		"32px"
					    	}

						});
						jQuery("#submit_worldpay_payment_form").click();
					});
				');
			} else {
				$woocommerce->add_inline_js('
					jQuery(function(){
						jQuery("body").block({

							message: "<img src=\"'.$woocommerce->plugin_url().'/assets/images/ajax-loader.gif\" alt=\"Redirecting...\" />'.__('Thank you for your order. We are now redirecting you to WorldPay to make payment.', 'woocommerce_worlday').'",
							overlayCSS:
							{
								background: "#fff",
								opacity: 0.6
							},
							css:
							{
					        	padding:        20,
						        textAlign:      "center",
						        color:          "#555",
						        border:         "3px solid #aaa",
						        backgroundColor:"#fff",
						        cursor:         "wait",
						        lineHeight:		"32px"
					    	}

						});
						jQuery("#submit_worldpay_payment_form").click();
					});
				');
			}
