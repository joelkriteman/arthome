<?php global $jck_wt; ?>

<?php if(!empty($images)) { ?>

    <div class="<?php echo $this->slug; ?>-images-wrap">

    	<div class="<?php echo $this->slug; ?>-images <?php if( $jck_wt['clickAnywhere'] ) echo $this->slug."-images--click_anywhere"; ?>">
    
        	<?php $i = 0; foreach($images as $image): ?>
        		
        		<div class="<?php echo $this->slug; ?>-images__slide <?php if($i == 0) echo $this->slug."-images__slide--active"; ?>" data-index="<?php echo $i; ?>">
        		
        			<img class="<?php echo $this->slug; ?>-images__image" src="<?php echo $image['single'][0]; ?>" data-large-image="<?php echo $image['large'][0]; ?>" data-large-image-width="<?php echo $image['large'][1]; ?>" title="<?php echo $image['title']; ?>" alt="<?php echo $image['alt']; ?>">
        		
        		</div>
        		
        	<?php $i++; endforeach; ?>
    	
    	</div>
    	
    	<div class="<?php echo $this->slug; ?>-loading-overlay"><i class="<?php echo $this->slug; ?>-icon-loading"></i></div>
	
    </div>

<?php } ?>